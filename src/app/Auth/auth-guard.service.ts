import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthService) {}

  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      console.log('oh no not logged in!');
      this.tryRefresh();
    }
    return true;
  }

  tryRefresh() {
    return this.auth.refreshToken().subscribe(() => true,
    () => this.auth.logout()
    );

  }

}
