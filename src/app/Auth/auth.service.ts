import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LoginUser } from '../manager/user/user.model';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()

export class AuthService {

    private readonly BaseUrl = environment.tokenEndpoint;

    constructor(private http: HttpClient, private router: Router) {  }

    // login(email: string, password: string ) {
    //     return this.http.post<LoginUser>('/api/login', {email, password})
    //         .do(res => this.setSession)
    //         .shareReplay();
    // }

    login(username, password): Observable<any> {
      const body = new HttpParams()
        .set('username', username)
        .set('password', password);

      const tokenUrl = this.BaseUrl + 'token';

      return this.http.post(tokenUrl,
                            body.toString(),
                            { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') }
                      )
                      .do(res => {
                        this.setSession(res);
                      })
                      .shareReplay();
    }

    private setSession(authResult) {
      console.log('in setSession');

      const expiresAt = moment().add(authResult.expires_in, 'second');

      localStorage.setItem('id_token', authResult.access_token);
      localStorage.setItem('refresh_token', authResult.refresh_token);
      localStorage.setItem('firstName', authResult.firstName);
      localStorage.setItem('lastName', authResult.lastName);
      localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()) );

      console.log('Bearer ' + localStorage.getItem('id_token'));
      console.log('Refresh ' + localStorage.getItem('refresh_token'));
      console.log('Expires ' + localStorage.getItem('expires_at'));
      console.log('First Name ' + localStorage.getItem('firstName'));
      console.log('Last Name ' + localStorage.getItem('lastName'));
    }

    logout() {
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('refresh_token');
        this.router.navigate(['/login']);
    }

    public isLoggedIn() {
        console.log('checking logged in against ' + this.getExpiration());
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem('expires_at');
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    refreshToken() {
      const refreshToken = localStorage.getItem('refresh_token');
      console.log('Refresh Token is ' + refreshToken);
      const body = new HttpParams().set('refreshToken', refreshToken);

      const refreshUrl = this.BaseUrl + 'refresh';
      console.log('Refresh Url is ' + refreshUrl);
      return this.http.post(refreshUrl,
                            body.toString(),
                            { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') }
                      )
                      .do(res => {
                        console.log('in do');
                        this.setSession(res);
                      })
                      .shareReplay();
    }

    getAuthToken() {
      return localStorage.getItem('id_token');
    }

    getFirstName() {
      return localStorage.getItem('firstName');
    }
}
