import { Injectable, Injector } from '@angular/core';
import { HttpRequest,
        HttpInterceptor,
        HttpHandler,
        HttpEvent,
        HttpErrorResponse,
        HttpUserEvent,
        HttpResponse,
        HttpProgressEvent,
        HttpHeaderResponse,
        HttpSentEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    isRefreshingToken = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private injector: Injector) {}

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        if (!this.isRefreshingToken) {
            console.log('attaching id token: ' + token);
            return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }});
        } else {
            req.headers.delete('Authorization');
            console.log('is refreshing token so no bearer token attached!');
            return req.clone();
        }
    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
            const authService = this.injector.get(AuthService);
            return next.handle(this.addToken(req, authService.getAuthToken()))
                .catch(error => {
                    if (error instanceof HttpErrorResponse) {
                        switch ((<HttpErrorResponse>error).status) {
                            case 400:
                                return this.handle400Error(error);
                            case 401:
                                return this.handle401Error(req, next);
                        }
                    } else {
                        return Observable.throw(error);
                    }
            });
    }

    handle400Error(error) {
        if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
            // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
            return this.logoutUser();
        }

        return Observable.throw(error);
    }

    handle401Error(req: HttpRequest<any>, next: HttpHandler) {
        console.log('in 401 error!');
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;

            const authService = this.injector.get(AuthService);
            // Reset here so that the following requests wait until the token
            // comes back from the refreshToken call.
            this.tokenSubject.next(null);

            return authService.refreshToken()
                .switchMap((newToken: string) => {
                    if (newToken) {
                        this.tokenSubject.next(newToken);
                        console.log('new bearer token is: Bearer' + newToken);
                        this.isRefreshingToken = false;
                        // this.addToken(req, authService.getAuthToken());
                        return next.handle(this.addToken(req, authService.getAuthToken()));
                    }

                    // If we don't get a new token, we are in trouble so logout.
                    return this.logoutUser();
                })
                .catch(error => {

                    // If there is an exception calling 'refreshToken', bad news so logout.
                    return this.logoutUser();
                })
                .finally(() => {
                    this.isRefreshingToken = false;
                });
        } else {
            return this.tokenSubject
                .filter(token => token != null)
                .take(1)
                .switchMap(token => {
                    return next.handle(this.addToken(req, token));
                });
        }
    }

    logoutUser() {
        // Route to the login page (implementation up to you)
        const authService = this.injector.get(AuthService);
        authService.logout();
        return Observable.throw('');
    }
}
