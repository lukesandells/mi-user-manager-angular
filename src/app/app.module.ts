import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ManagerComponent } from './manager/manager.component';
import { UserComponent } from './manager/user/user.component';
import { RoleComponent } from './manager/role/role.component';
import { PermissionComponent } from './manager/permission/permission.component';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './manager/user/create-user/create-user.component';
import { RoleDetailComponent } from './manager/role/role-detail/role-detail.component';
import { UserDetailComponent } from './manager/user/user-detail/user-detail.component';
import { PermissionDetailComponent } from './manager/permission/permission-detail/permission-detail.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { UserService } from './manager/user/user.service';
import { MiUserManagerAppDataService } from './shared/mi-user-manager-app-data.service';
import { EditUserComponent } from './manager/user/edit-user/edit-user.component';
import { RoleTemplateService } from './manager/role/role.service';
import { PermissionTemplateService } from './manager/permission/permission.service';
import { AuthService } from './Auth/auth.service';
import { AuthInterceptor } from './Auth/AuthInterceptor';
import { AuthGuardService } from './Auth/auth-guard.service';
import { EditPermissionTemplateComponent } from './manager/permission/edit-permission/edit-permission.component';
import { CreatePermissionTemplateComponent } from './manager/permission/create-permission/create-permission.component';
import { EditRoleTemplateComponent } from './manager/role/edit-role/edit-role.component';
import { CreateRoleTemplateComponent } from './manager/role/create-role/create-role.component';
import { ParameterViewComponent } from './manager/shared/parameters/parameter-view/parameter-view.component';
import { PermissionViewComponent } from './manager/shared/permission/permission-view/permission-view.component';
import { ParametersService } from './manager/shared/parameters/parameters.service';
import { RolesViewComponent } from './manager/shared/roles/roles-view/roles-view.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetPasswordService } from './reset-password/reset-password.service';
import { RoleArrayComponent } from './manager/shared-components/role-array/role-array.component';
import { RoleItemComponent } from './manager/shared-components/role-item/role-item.component';
import { ParameterComponent } from './manager/shared-components/parameter/parameter.component';
import { ParameterArrayComponent } from './manager/shared-components/parameter-array/parameter-array.component';
import { PermissionItemComponent } from './manager/shared-components/permission-item/permission-item.component';
import { PermissionArrayComponent } from './manager/shared-components/permission-array/permission-array.component';
import { UserManagerBaseComponent } from './manager/shared/componentbase/user-manager-base/user-manager-base.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    HeaderComponent,
    FooterComponent,
    ManagerComponent,
    UserComponent,
    RoleComponent,
    PermissionComponent,
    LoginComponent,
    CreateUserComponent,
    RoleDetailComponent,
    UserDetailComponent,
    PermissionDetailComponent,
    EditUserComponent,
    EditPermissionTemplateComponent,
    CreatePermissionTemplateComponent,
    EditRoleTemplateComponent,
    CreateRoleTemplateComponent,
    ParameterViewComponent,
    PermissionViewComponent,
    RolesViewComponent,
    ResetPasswordComponent,
    RoleComponent,
    ParameterComponent,
    ParameterArrayComponent,
    RoleArrayComponent,
    RoleItemComponent,
    PermissionItemComponent,
    PermissionArrayComponent,
    UserManagerBaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    UserService,
    RoleTemplateService,
    PermissionTemplateService,
    MiUserManagerAppDataService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthGuardService,
    ParametersService,
    ResetPasswordService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
