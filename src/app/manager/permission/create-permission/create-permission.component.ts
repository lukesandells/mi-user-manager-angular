import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionTemplateService } from '../permission.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { ParamViewModel, PermissionViewModel } from '../../shared/param.model';
import { PermissionTemplateBaseComponent } from '../permission-base/permission-base.component';
import { ParametersService } from '../../shared/parameters/parameters.service';

@Component({
  selector: 'app-create-permission',
  templateUrl: '../permission-form/permission-form.html',
  styleUrls: ['./create-permission.component.css']
})
export class CreatePermissionTemplateComponent extends PermissionTemplateBaseComponent implements OnInit {
    complete = true;
    pageTitle = 'Create';
    persisted: string = null;
    isCreate: false;

  constructor(protected route: ActivatedRoute,
    protected ptSvc: PermissionTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected paramsSvc: ParametersService) {
      super(route, ptSvc, router, fb, location, paramsSvc);
     }

  ngOnInit() {
    super.ngOnInit();
  }

  onEditPermissionTemplate() {
    // console.log('reactive form submitted');

    const newPvm = new PermissionViewModel();
    newPvm.internalId = '';
    newPvm.name = this.name.value;
    newPvm.description = this.description.value;
    newPvm.claimType = this.claimType.value;
    newPvm.parameters = [];

    this.permissionTemplate = newPvm;

    // this.permissionTemplate =  InternalId:  '',
    // Name: this.name.value,
    // Description: this.description.value,
    // ClaimType: this.claimType.value,
    // Parameters: null
    // };

    this.form.patchValue({
      name: this.permissionTemplate.name,
      description: this.permissionTemplate.description,
      claimType: this.permissionTemplate.claimType
    });

    this.ptSvc.createPermissionTemplate(this.form.value)
      .subscribe((data) => console.log(data),
                  err => { console.log(err); this.persisted = 'Error saving new permission template, contact an administrator';},
                  () => this.persisted = 'New permission template saved'
      );
  }
}
