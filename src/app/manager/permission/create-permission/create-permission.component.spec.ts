import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePermissionTemplateComponent } from './create-permission.component';

describe('CreatePermissionTemplateComponent', () => {
  let component: CreatePermissionTemplateComponent;
  let fixture: ComponentFixture<CreatePermissionTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePermissionTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePermissionTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
