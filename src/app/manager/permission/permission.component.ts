import { Component, OnInit } from '@angular/core';
import {PermissionTemplateService} from './permission.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import { PermissionViewModel } from '../shared/param.model';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent implements OnInit {

  permissionTemplates: PermissionViewModel[] = new Array<PermissionViewModel>();
  hasExistingPermissionTemplates = false;
  complete: boolean;
  // permissionTemplate: PermissionTemplate;

  constructor(private ptSvc: PermissionTemplateService) {
  }

  ngOnInit() {
    this.ptSvc.getPermissionTemplates().subscribe((tempdate) => {
      this.permissionTemplates = tempdate;
      this.hasExistingPermissionTemplates = tempdate.length > 0;
    },
    err => console.log(err),
    () => {
      this.complete = true;
      this.hasExistingPermissionTemplates = this.hasPermissionTemplates();
      }
    );
  }

  hasPermissionTemplates() {
    return this.permissionTemplates.length > 0;
  }

  onPermissionTemplateDelete(rtInternalId: string) {
    this.ptSvc.deletePermissionTemplate(rtInternalId).subscribe(() => {
      console.log('success');
      },
      () => {
      console.log('failed');
      });
  }

}
