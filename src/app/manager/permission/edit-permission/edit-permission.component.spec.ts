import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPermissionTemplateComponent } from './edit-permission.component';

describe('EditPermissionTemplateComponent', () => {
  let component: EditPermissionTemplateComponent;
  let fixture: ComponentFixture<EditPermissionTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPermissionTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPermissionTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
