import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionTemplateService } from '../permission.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { PermissionViewModel, ParamViewModel } from '../../shared/param.model';
import { PermissionTemplateBaseComponent } from '../permission-base/permission-base.component';
import { ParametersService } from '../../shared/parameters/parameters.service';

@Component({
  selector: 'app-edit-permission',
  templateUrl: '../permission-form/permission-form.html',
  styleUrls: ['./edit-permission.component.css']
})
export class EditPermissionTemplateComponent extends PermissionTemplateBaseComponent implements OnInit {
  // ptSubscription: Subscription;
  // permissionTemplate: PermissionViewModel;
  // name = new FormControl('', Validators.required);
  // claimType = new FormControl('', Validators.required);
  // description = new FormControl('', Validators.required);
  // form: FormGroup;
  // paramsSub: Subscription;
  // complete = false;
  // ptInternalId: string;
  pageTitle = 'Edit';
  // persisted: string = null;
  parameters: ParamViewModel[];
  isCreate: false;

  constructor(protected route: ActivatedRoute,
    protected ptSvc: PermissionTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected paramsSvc: ParametersService) {
      super(route, ptSvc, router, fb, location, paramsSvc);
      this.ptInternalId = this.route.snapshot.params['internalId'];

  }

  ngOnInit() {
    super.ngOnInit();
    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.ptInternalId = parameters['internalId'];
    });

    console.log('InternalID in init is ' + this.ptInternalId);

    this.getPermissionTemplate(this.ptInternalId);
  }

  // getPermissionTemplate(internalId: string) {
  //   this.ptSvc.getPermissionTemplate(internalId)
  //     .subscribe(
  //       (pt: PermissionViewModel) => {
  //         this.onPermissionTemplateRetrieved(pt);
  //       },
  //       error =>  console.log(error),
  //       () => this.complete = true
  //   );
  // }

  // onPermissionTemplateRetrieved(pt: PermissionViewModel): void {
  //   if (this.form) {
  //     this.form.reset();
  //   }
  //   this.permissionTemplate = pt;
  //   this.parameters = pt.parameters;

  //   this.form.patchValue({
  //       name: this.permissionTemplate.name,
  //       claimType: this.permissionTemplate.claimType,
  //       description: this.permissionTemplate.description,
  //       ptInternalId: this.ptInternalId
  //   });
  // }

  onEditPermissionTemplate() {
    console.log('reactive form submitted');
    this.ptSvc.editPermissionTemplate(this.ptInternalId, this.form.value)
      .subscribe((data) => console.log(data),
                  err => { console.log(err); this.persisted = 'Error saving changes - contact an administrator'; },
                  () => this.persisted = 'Changes saved'
      );
  }

  // onReset() {
  //   this.form.reset();
  // }

  // onCancel() {
  //   this.location.back();
  // }
}
