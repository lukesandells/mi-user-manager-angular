import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { PermissionTemplateService } from '../permission.service';
import { PermissionViewModel } from '../../shared/param.model';

@Component({
  selector: 'app-permission-detail',
  templateUrl: './permission-detail.component.html',
  styleUrls: ['./permission-detail.component.css']
})
export class PermissionDetailComponent implements OnInit {
  ptInternalId: string;
  permissionTemplate: PermissionViewModel;
  paramsSub: Subscription;
  complete = false;

  constructor(private route: ActivatedRoute,
              private ptSvc: PermissionTemplateService,
              private router: Router) {
    this.ptInternalId = this.route.snapshot.params['internalId'];
  }

  ngOnInit() {
    this.ptInternalId = this.route.snapshot.params['internalId'];

    this.paramsSub = this.route.params.subscribe((parameters) => {
      this.ptInternalId = parameters['internalId'];
    });

    this.getPermissionTemplate(this.ptInternalId);
  }

  getPermissionTemplate(internalId: string) {
    this.ptSvc.getPermissionTemplate(internalId)
      .subscribe(
        (pt: PermissionViewModel) => {
          this.onPermissionTemplateRetrieved(pt);
        },
        error =>  console.log(error), // err arg
        () => this.complete = true // complete arg
    );
  }


  onPermissionTemplateRetrieved(permissionTemplate: PermissionViewModel): void {
    this.permissionTemplate = permissionTemplate;
    console.log('Permission Template is ' + this.permissionTemplate.name);
  }

}
