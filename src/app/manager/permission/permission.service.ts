import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';

import { PermissionViewModel, ParamViewModel } from '../shared/param.model';

const httpOptions = {
    headers: new HttpHeaders ({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
};

@Injectable()

export class PermissionTemplateService implements OnInit {
    private readonly BaseUrl = environment.userManagerEndpoint + 'PermissionTemplate';
    constructor(private httpClient: HttpClient) {

    }

    ngOnInit() { }

    getAll() {
        return this.httpClient.get<PermissionViewModel[]>(this.BaseUrl);
    }

    getAllUnassociatedWithRole(roleInternalId: string) {
        return this.httpClient.get<PermissionViewModel[]>(this.BaseUrl + '/GetFilteredByRoleInternalId/' + roleInternalId);
    }

    getAllUnassociatedWithUser(userInternalId: string) {
        console.log('FML unassoc with User');
        return this.httpClient.get<PermissionViewModel[]>(this.BaseUrl + '/GetFilteredByUserInternalId/' + userInternalId);
      }

    getPermissionTemplate(internalId: string) {
        return this.httpClient.get<PermissionViewModel>(this.BaseUrl + '/' + internalId);
    }

    deletePermissionTemplate(rtInternalId: string) {
        console.log('deleting permission template ID ' + rtInternalId);
        const params = new HttpParams().set('internalId', rtInternalId);
        const headers = httpOptions.headers;

        return this.httpClient.delete<PermissionViewModel>(this.BaseUrl + '/' + rtInternalId, {headers, params});
    }

    createPermissionTemplate(permissionTemplate: PermissionViewModel) {
        console.log('adding permission template ' + permissionTemplate.name);
        const headers = new HttpHeaders().set('content-type', 'application/json');
        const body = permissionTemplate;
        return this.httpClient.post<PermissionViewModel>(this.BaseUrl, body, {headers});
    }

    editPermissionTemplate(rtInternalId: string, permissionTemplate: PermissionViewModel) {
        console.log('editing permission template, name=' + permissionTemplate.name);
        const params = new HttpParams().set('internalId', rtInternalId);
        const headers = new HttpHeaders().set('content-type', 'application/json');

        // TEMP: While Edit Permission Template doesn't display parameters, we can't submit a properly formed JSON with GUIDs
        // hence the nonsense below...
        // const prm: ParamViewModel[] = [new ParamViewModel()];
        // prm[0].name = '*';
        // prm[0].internalId = '4925DBA0-EB58-4F28-86CB-E55D060C2332';
        // prm[0].description = '*';
        // prm[0].typeInternalId = '4925DBA0-EB58-4F28-86CB-E55D060C2332';
        // prm[0].typeName = '*';
        // prm[0].value = '*';
        // permissionTemplate.internalId = rtInternalId;
        // permissionTemplate.parameters = prm;
        const body = permissionTemplate;
        console.log(JSON.stringify(body));
        return this.httpClient.put<PermissionViewModel>(this.BaseUrl + '/' + rtInternalId, body, {headers});
    }

    getPermissionTemplates() {
        console.log('getting Permission templates from ' + this.BaseUrl);
        return this.httpClient.get<PermissionViewModel[]>(this.BaseUrl);
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred: ', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }

        return new ErrorObservable(
            'Something bad happened; please try again later.');
    }
}
