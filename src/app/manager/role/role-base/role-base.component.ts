import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleTemplateService } from '../role.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { ParamViewModel, RoleViewModel, PermissionViewModel, ParamType } from '../../shared/param.model';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { PermissionTemplateService } from '../../permission/permission.service';
import { PermissionTemplateBaseComponent } from '../../permission/permission-base/permission-base.component';
import { UserManagerBaseComponent } from '../../shared/componentbase/user-manager-base/user-manager-base.component';

@Component({
  selector: 'app-edit-role',
  templateUrl: '../role-form/role-form.html',
  styleUrls: ['./role-base.component.css']
})
export class RoleTemplateBaseComponent extends UserManagerBaseComponent implements OnInit {

 // paramTypes: ParamType[];
  existingPermissionsArray: PermissionViewModel[];
  roleTemplate: RoleViewModel;
  name = new FormControl('', Validators.required);
  description = new FormControl('', Validators.required);
  internalId = new FormControl('');
  roleInternalId: string;
 // form: FormGroup;
  paramsSub: Subscription;
 // complete = false;
  pageTitle = 'Edit';
  persisted: string = null;
  isCreate = false;

  constructor(protected route: ActivatedRoute,
    protected rtSvc: RoleTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected paramsSvc: ParametersService,
    protected permSvc: PermissionTemplateService) {
    super(location, paramsSvc);
    this.roleInternalId = this.route.snapshot.params['internalId'];
  }

  ngOnInit() {
    super.ngOnInit();
    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.roleInternalId = parameters['internalId'];
    });
    this.getExistingPermissions();
    this.getRoleTemplateParameterTypes();
    this.createForm();

    console.log('getting role with Guid ' + this.roleInternalId);
    console.log('bleck');
    // this.getRoleTemplate(this.roleInternalId);
  }

  createForm() {
    this.form = this.fb.group({
      name: this.name,
      description: this.description,
      internalId: this.internalId
    });
  }

  getRoleTemplate(internalId: string) {
    this.rtSvc.getRoleTemplate(internalId)
      .subscribe(
        (rt: RoleViewModel) => {
          this.onRoleTemplateRetrieved(rt);
        },
        error =>  console.log(error),
        () => this.complete = true
    );
  }

  onRoleTemplateRetrieved(rt: RoleViewModel): void {
    if (this.form) {
      this.form.reset();
    }
    this.roleTemplate = rt;

    this.form.patchValue({
        name: this.roleTemplate.name,
        description: this.roleTemplate.description,
        internalId: this.roleInternalId,
        // permissions: this.permissions,
        // parameters: this.parameters,
    });
    // this.parameters = rt.parameters ? rt.parameters : [];
    // this.permissions = rt.permissions ? rt.permissions : [];
  }

  onEditRoleTemplate() {
    let formData = this.form.value;

    const roleParams = formData['parameters'];
    const rolePerms = formData['permissions'];
    console.log('formData.permissions: ' + JSON.stringify(rolePerms));
    console.log('formData.parameters: ' + JSON.stringify(roleParams));

    const vm = [new PermissionViewModel()];

    let i = 0;
    for (const p of rolePerms) {
      const tempParameters = p['parameters'];

      vm[i].claimType = p['claimType'];
      vm[i].description = p['description'];
      vm[i].internalId = this.roleInternalId; // p['internalId'];
      vm[i].name = p['name'];

      for (const prm of tempParameters) {
        for (const param of roleParams) {
          if (param.internalId === prm) {
            vm[i].parameters.push(param);
          }
        }
      }

      const formArray = (<FormArray>this.form.controls['permissions']).at(i);
            formArray.patchValue({
              name: vm[i].name,
              internalId: vm[i].internalId,
              description: vm[i].description,
              claimType: vm[i].claimType,
              parameters: vm[i].parameters
            });

      console.log('permission ' + i + ': ' + JSON.stringify(vm[i]));

      i++;
      vm.push(new PermissionViewModel());
    }

    formData = this.form.value;
    delete formData['existingPermissions'];
    delete formData['paramTypes'];

    console.log('reactive form submitted ' + JSON.stringify(formData) );

    this.rtSvc.editRoleTemplate(this.roleInternalId, formData)
      .subscribe((data) => console.log(data),
                  err => { console.log(err); this.persisted = 'Error saving changes - contact an administrator'; },
                  () => this.persisted = 'Changes saved'
      );
  }

  // onReset() {
  //   this.form.reset();
  // }

  // onCancel() {
  //   this.location.back();
  // }

  getExistingPermissions() {
    // console.log('BLURGH:' + this.parentInternalId);
    let obs: Observable<PermissionViewModel[]>;

    if (this.roleInternalId) {
        obs = this.permSvc.getAllUnassociatedWithRole(this.roleInternalId);
    } else {
      obs = this.permSvc.getAll();
    }

    console.log('in GETEXISTINGPERMS');
    obs.subscribe((existingPermissions: PermissionViewModel[]) => {
          this.onExistingPermissionsRetrieved(existingPermissions);
        },
        error => {
          this.existingPermissionsArray = [];
          console.log(error); },
        () => this.complete = true
    );
  }

  onExistingPermissionsRetrieved(existingPermissions: PermissionViewModel[]): void {
    this.existingPermissionsArray = existingPermissions;
    console.log('existing perms ' + this.existingPermissionsArray.length);
    // this.hasUnassignedPermissions = existingPermissions.length > 0;
    // this.listExistingPermissions();
  }

  // getRoleTemplateParameterTypes() {
  //   this.paramsSvc.getRoleTemplateParameterTypes()
  //     .subscribe(
  //       (ptype: ParamType[]) => {
  //         this.onRoleTemplateParameterTypesRetrieved(ptype);
  //       },
  //       error =>  console.log(error),
  //       () => this.complete = true
  //   );
  // }

  // onRoleTemplateParameterTypesRetrieved(ptype: ParamType[]): void {
  //   this.paramTypes = ptype;
  // }

}
