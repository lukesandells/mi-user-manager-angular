import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleTemplateService } from '../role.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { ParamViewModel, RoleViewModel, PermissionViewModel } from '../../shared/param.model';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { Subject } from 'rxjs/Subject';
import { RoleTemplateBaseComponent } from '../role-base/role-base.component';
import { PermissionTemplateService } from '../../permission/permission.service';

@Component({
  selector: 'app-edit-role',
  templateUrl: '../role-form/role-form.html',
  styleUrls: ['./edit-role.component.css']
})
export class EditRoleTemplateComponent extends RoleTemplateBaseComponent implements OnInit {

  complete = false;
  pageTitle = 'Edit';
  persisted: string = null;
  isCreate = false;

  constructor(protected route: ActivatedRoute,
    protected rtSvc: RoleTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected ptSvc: ParametersService,
    protected permSvc: PermissionTemplateService) {
    // call base class constructor
    super(route, rtSvc, router, fb, location, ptSvc, permSvc);
    this.roleInternalId = this.route.snapshot.params['internalId'];
  }

  ngOnInit() {
    super.ngOnInit();
    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.roleInternalId = parameters['internalId'];
    });

    this.createForm();

    console.log('getting role with Guid ' + this.roleInternalId);
    this.getRoleTemplate(this.roleInternalId);
  }

  onEditRoleTemplate() {
    let formData = this.form.value;

    const roleParams = formData['parameters'];
    const rolePerms = formData['permissions'];
    console.log('formData.permissions: ' + JSON.stringify(rolePerms));
    console.log('formData.parameters: ' + JSON.stringify(roleParams));

    const vm = [new PermissionViewModel()];

    let i = 0;
    for (const p of rolePerms) {
      const tempParameters = p['parameters'];

      vm[i].claimType = p['claimType'];
      vm[i].description = p['description'];
      vm[i].internalId = this.roleInternalId; // p['internalId'];
      vm[i].name = p['name'];

      for (const prm of tempParameters) {
        for (const param of roleParams) {
          if (param.internalId === prm) {
            if (!param.typeName) {
              param.typeName = '';
            }

            vm[i].parameters.push(param);
          }
        }
      }

      const formArray = (<FormArray>this.form.controls['permissions']).at(i);
            formArray.patchValue({
              name: vm[i].name,
              internalId: vm[i].internalId,
              description: vm[i].description,
              claimType: vm[i].claimType,
              parameters: vm[i].parameters
            });

      console.log('permission ' + i + ': ' + JSON.stringify(vm[i]));

      i++;
      vm.push(new PermissionViewModel());
    }

    formData = this.form.value;
    delete formData['existingPermissions'];
    delete formData['paramTypes'];

    console.log('reactive form submitted ' + JSON.stringify(formData) );

    this.rtSvc.editRoleTemplate(this.roleInternalId, formData)
      .subscribe((data) => console.log(data),
                  err => { console.log(err); this.persisted = 'Error saving changes - contact an administrator'; },
                  () => this.persisted = 'Changes saved'
      );
  }

}
