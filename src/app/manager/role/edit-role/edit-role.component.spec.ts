import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRoleTemplateComponent } from './edit-role.component';

describe('EditRoleTemplateComponent', () => {
  let component: EditRoleTemplateComponent;
  let fixture: ComponentFixture<EditRoleTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRoleTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRoleTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
