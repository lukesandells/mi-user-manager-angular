import { Component, OnInit } from '@angular/core';
import {RoleTemplateService} from './role.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import { ParametersService } from '../shared/parameters/parameters.service';
import { ParamType, RoleViewModel } from '../shared/param.model';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  roleTemplates: RoleViewModel[] = new Array<RoleViewModel>();
  hasExistingRoleTemplates = false;
  complete: boolean;
  roleTemplate: RoleViewModel;
  // paramTypesSub: Subscription;

  constructor(private rtSvc: RoleTemplateService) {
  }

  ngOnInit() {
    this.rtSvc.getRoleTemplates().subscribe((tempdate) => {
      this.roleTemplates = tempdate;
      this.hasExistingRoleTemplates = tempdate.length > 0;
    },
    err => console.log(err),
    () => {
      this.complete = true;
      this.hasExistingRoleTemplates = this.hasRoleTemplates();
      }
    );
  }

  hasRoleTemplates() {
    return this.roleTemplates.length > 0;
  }

  onRoleTemplateDelete(rtInternalId: string) {
    this.rtSvc.deleteRoleTemplate(rtInternalId).subscribe(() => {
      console.log('success');
      },
      () => {
      console.log('failed');
      });
  }
}
