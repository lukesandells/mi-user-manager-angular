import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleTemplateService } from '../role.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { ParamViewModel, RoleViewModel, PermissionViewModel } from '../../shared/param.model';
import { Subject } from 'rxjs/Subject';
import { RoleTemplateBaseComponent } from '../role-base/role-base.component';
import { PermissionTemplateService } from '../../permission/permission.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-create-role',
  templateUrl: '../role-form/role-form.html',
  styleUrls: ['./create-role.component.css']
})
export class CreateRoleTemplateComponent extends RoleTemplateBaseComponent implements OnInit {

    complete = true;
    pageTitle = 'Create';
    isCreate = true;
    persisted: string = null;

  constructor(protected route: ActivatedRoute,
    protected rtSvc: RoleTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected ptSvc: ParametersService,
    protected permSvc: PermissionTemplateService) {
      // call base class constructor
      super(route, rtSvc, router, fb, location, ptSvc, permSvc);
    }

  ngOnInit() {
      super.ngOnInit();
      console.log('FML');
      this.roleInternalId = environment.emptyGuid;
      this.internalId.patchValue(environment.emptyGuid);
  }


  onEditRoleTemplate() {


    const roleParams = this.form.value['parameters'];
    const rolePerms = this.form.value['permissions'];

    const vm = [new PermissionViewModel()];

    let i = 0;
    // for each permission in this role
    for (const p of rolePerms) {
      const tempParameters = p['parameters'];

      vm[i].claimType = p['claimType'];
      vm[i].description = p['description'];
      vm[i].internalId = p['internalId'];
      vm[i].name = p['name'];

      // // for each parameter in the permission
      // for (const prm of tempParameters) {
      //   // for each parameter in the role array
      //   for (const param of roleParams) {
      //     // check if the parameter exists in role and permission parameter arrays
      //     if (param.internalId === prm) {
      //       if (!param.typeName) {
      //         param.typeName = '';
      //       }
      //       vm[i].parameters.push(param);
      //     }
      //   }
      // }

      const formArray = (<FormArray>this.form.controls['permissions']).at(i);
            formArray.patchValue({
              name: vm[i].name,
              internalId: vm[i].internalId,
              description: vm[i].description,
              claimType: vm[i].claimType,
              parameters: vm[i].parameters
            });

      console.log('permission ' + i + ': ' + JSON.stringify(vm[i]));

      i++;
      vm.push(new PermissionViewModel());
    }

    this.roleTemplate = {
                  internalId:  this.internalId.value,
                  name: this.name.value,
                  description: this.description.value,
                  parameters: this.form.value['parameters'],
                  permissions: this.form.value['permissions'],
    };

    // for (const param of this.roleTemplate.parameters) {
    //   if (!param.typeName) {
    //     param.typeName = '';
    //   }
    // }

    console.log('reactive form submitted ' + JSON.stringify(this.form.value) );

    const formData = this.form.value;
    delete formData['existingPermissions'];
    delete formData['paramTypes'];

    console.log('after delete: ' + JSON.stringify(formData));

    this.rtSvc.createRoleTemplate(formData)
      .subscribe((data) => {
        console.log(data);
        this.persisted = 'Role template saved, you will redirected to the edit page now...';
        setTimeout(() => this.location.go('/role/edit/' + data), 500);
      },
      err => { console.log(err); this.persisted = 'Error saving new role template, contact an administrator'; },
      () => this.persisted = 'New role template saved'
    );
  }

}
