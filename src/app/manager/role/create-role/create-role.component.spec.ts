import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRoleTemplateComponent } from './create-role.component';

describe('CreateRoleTemplateComponent', () => {
  let component: CreateRoleTemplateComponent;
  let fixture: ComponentFixture<CreateRoleTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRoleTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoleTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
