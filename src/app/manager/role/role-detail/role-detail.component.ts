import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RoleTemplateService } from '../role.service';
import { ParameterViewComponent } from '../../shared/parameters/parameter-view/parameter-view.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RoleViewModel } from '../../shared/param.model';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.css']
})
export class RoleDetailComponent implements OnInit {
  rtInternalId: string;
  roleTemplate: RoleViewModel;
  paramsSub: Subscription;
  complete = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private rtSvc: RoleTemplateService,
              private router: Router,
              private fb: FormBuilder) {
    this.rtInternalId = this.route.snapshot.params['internalId'];
    this.form = this.fb.group([]);
  }

  ngOnInit() {
    this.rtInternalId = this.route.snapshot.params['internalId'];

    this.paramsSub = this.route.params.subscribe((parameters) => {
      this.rtInternalId = parameters['internalId'];
    });

    this.getRoleTemplate(this.rtInternalId);
  }

  getRoleTemplate(internalId: string) {
    this.rtSvc.getRoleTemplate(internalId)
      .subscribe(
        (rt: RoleViewModel) => {
          this.onRoleTemplateRetrieved(rt);
        },
        error =>  console.log(error), // err arg
        () => this.complete = true // complete arg
    );
  }


  onRoleTemplateRetrieved(roleTemplate: RoleViewModel): void {
    this.roleTemplate = roleTemplate;
    console.log('Role Template is ' + this.roleTemplate.name);
  }

}
