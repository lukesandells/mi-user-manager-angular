import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import { RoleViewModel, ParamViewModel } from '../shared/param.model';

const httpOptions = {
    headers: new HttpHeaders ({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
};

@Injectable()

export class RoleTemplateService implements OnInit {

    private readonly BaseUrl = environment.userManagerEndpoint + 'RoleTemplate';
    roleTemplatesSub: Subscription;
    roleTemplates: Observable<RoleViewModel[]>;

    constructor(private httpClient: HttpClient) {

    }

    ngOnInit() { }

    getAll() {
        return this.httpClient.get<RoleViewModel[]>(this.BaseUrl);
    }

    getAllUnassociatedWithUser(userInternalId: string) {
        console.log('in get by filetered blah');
        return this.httpClient.get<RoleViewModel[]>(this.BaseUrl + '/GetFilteredByUserInternalId/' + userInternalId);
      }

    getRoleTemplate(internalId: string) {
        return this.httpClient.get<RoleViewModel>(this.BaseUrl + '/' + internalId);
    }

    deleteRoleTemplate(rtInternalId: string) {
        console.log('deleting role template');
        const params = new HttpParams().set('internalId', rtInternalId);
        const headers = httpOptions.headers;

        return this.httpClient.delete<RoleViewModel>(this.BaseUrl + '/' + rtInternalId, {headers, params});
    }

    createRoleTemplate(roleTemplate: RoleViewModel) {
        console.log('adding role template ' + roleTemplate.name);

        const headers = new HttpHeaders().set('content-type', 'application/json');

        // temp: add an empty roletemplate::permission::permissiontemplateparameter
        const tempParam: ParamViewModel = new ParamViewModel;
        const body = roleTemplate;

        console.log('aaaa' + JSON.stringify(body));

        return this.httpClient.post<RoleViewModel>(this.BaseUrl, body, {headers});
    }

    editRoleTemplate(rtInternalId: string, roleTemplate: RoleViewModel) {
        console.log('editing role template');
        const params = new HttpParams().set('internalId', rtInternalId);
        const headers = new HttpHeaders().set('content-type', 'application/json');
        const body = roleTemplate;
        // console.log(JSON.stringify(body));
        return this.httpClient.put<RoleViewModel>(this.BaseUrl + '/' + rtInternalId, body, {headers});
    }

    getRoleTemplates() {
        console.log('getting role templates from ' + this.BaseUrl);
        return this.httpClient.get<RoleViewModel[]>(this.BaseUrl);
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred: ', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }

        return new ErrorObservable(
            'Something bad happened; please try again later.');
    }
}
