import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleArrayComponent } from './role-array.component';

describe('RoleArrayComponent', () => {
  let component: RoleArrayComponent;
  let fixture: ComponentFixture<RoleArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
