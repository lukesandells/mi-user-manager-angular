import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { RoleViewModel, ParamType, SiteViewModel } from '../../shared/param.model';

@Component({
  selector: 'app-role-array',
  templateUrl: './role-array.component.html',
  styleUrls: ['./role-array.component.css']
})
export class RoleArrayComponent implements OnInit {

  hasUnassignedRoles: boolean;
  @Input() parentForm: FormGroup;
  @Input() roles: RoleViewModel[];
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() existingRolesArray: RoleViewModel[] = [];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
      this.parentForm.addControl('roles', new FormArray([]));
      this.parentForm.addControl('roleTemplates', new FormControl());
      if (!this.roles) {
        this.roles = new Array<RoleViewModel>();
      }
      this.hasUnassignedRoles = this.existingRolesArray.length > 0;
  }

  // addRole(index: number) {
  //     this.roles.push({
  //         internalId: '',
  //         name: '',
  //         description: '',
  //         parameters: [],
  //         permissions: [],
  //     });
  // }

  removeRole(index: number) {
    console.log('trying to remove at ' + index);
      if (this.roles.length >= 1) {
          const role = this.roles[index];
          this.roles.splice(index, 1);
          (<FormArray>this.parentForm.get('roles')).removeAt(index);
          this.existingRolesArray.push(role);
          this.hasUnassignedRoles = this.existingRolesArray.length > 0;
      }
  }

  onAddExistingRole(roleId: string) {
    // console.log('EXISTING: ' + roleId);
    const role = this.existingRolesArray.find(r => r.internalId === roleId);
    this.roles.push(role);
    this.existingRolesArray = this.existingRolesArray.filter(r => r.internalId !== roleId);
    this.hasUnassignedRoles = this.existingRolesArray.length > 0;
  }

}
