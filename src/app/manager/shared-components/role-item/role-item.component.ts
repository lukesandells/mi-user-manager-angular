import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { RoleViewModel, ParamType, SiteViewModel, PermissionViewModel } from '../../shared/param.model';

const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'app-role-item',
  templateUrl: './role-item.component.html',
  styleUrls: ['./role-item.component.css']
})
export class RoleItemComponent implements OnInit {

  @Input() formArray: FormArray;
  @Input() role: RoleViewModel;
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() permissions: string;

  roleGroup: FormGroup;
  index: number;

  @Output() removed = new EventEmitter();

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
      this.roleGroup = this.toFormGroup(this.role);
      console.log('ROLE is ' + this.role.name + ', it has ' + this.role.parameters.length + ' params');
      resolvedPromise.then(() => {
          this.index = this.formArray.length;
          this.formArray.push(this.roleGroup);
      });
  }

  toFormGroup(role: RoleViewModel) {
      return this.fb.group({
          internalId: role.internalId,
          name: role.name,
          description: role.description,
          permissions: JSON.stringify(role.permissions)
      });
  }

}
