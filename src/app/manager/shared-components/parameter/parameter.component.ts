import { Component, OnInit, ChangeDetectorRef, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { ParamViewModel, ParamType, SiteViewModel } from '../../shared/param.model';


const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  siteParameterTypeId: string;
  @Input() formArray: FormArray;
  @Input() parameter: ParamViewModel;
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() parentIsUser ? = false;
  index: number;

  selectedSites: SiteViewModel[] = [];

  @Output() removed = new EventEmitter();
  parameterGroup: FormGroup;

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.parameterGroup = this.toFormGroup(this.parameter);

    // if (this.parentIsUser && param.typeInternalId === this.siteParameterTypeId) {
    //   const siteValues: string[] = [ param.value ];
    //   formArray.patchValue({value: siteValues});
    // }

    // if (this.parameter.value && this.parameter.typeName === 'SiteParam') {

    //   const selectedSites: string[] = [this.parameter.value];
    //   formArray.patchValue({value: selectedSites});
    // }

    if (this.sites) {
      this.siteParameterTypeId = this.paramTypes.find(pt => pt.Name === 'SiteParam').InternalId;
    }

    console.log('THIS PARAM IS OF TYPE: ' + this.parameter.typeName + ' with VALUE ' + this.parameter.value);

    resolvedPromise.then(() => {
      this.index = this.formArray.length;
      this.formArray.push(this.parameterGroup);
    });
  }

  setSelected(selectElement) {
    this.selectedSites = [];
    for (let i = 0; i < selectElement.options.length; i++) {
      const optionElement = selectElement.options[i];
      const optionModel = this.sites[i - 1];

      if (optionElement.selected === true) {
        console.log('selected = ' + JSON.stringify(optionModel));

        this.selectedSites.push(optionModel);
      } else {
        const index = this.selectedSites.indexOf(optionModel, 0);
        if (index > -1) {
          this.selectedSites.splice(index, 1);
        }
      }
    }

    this.parameter.value = this.convertSelectedSitesToString(this.selectedSites);
    console.log('this ' + this.parameter.value);
  }

  convertSelectedSitesToString(selectedSites: SiteViewModel[]) {
    let j = 0;
    let finalValue = '';
    for (const s of selectedSites) {
      const val: string = s['InternalId'];
      if (j !== 0) {
        finalValue += ',';
      }
      finalValue += val;
      j++;
    }
    return finalValue;
  }

  convertSiteIdsToSiteArray(returnedSites: string, siteList: SiteViewModel[]) {
    const ret: string[] = [];
    returnedSites += ','; 
    if (returnedSites && returnedSites.includes(',')) {
      returnedSites.split(',').forEach((s) => {
        ret.push(s);
        console.log('adding to array: ' + s);
      });
    }

    return ret;
  }

  toFormGroup(param: ParamViewModel) {
    const val = (param.typeName === 'SiteParam' ? this.convertSiteIdsToSiteArray(param.value, this.sites) : param.value);
    console.log('highlight these: ' + JSON.stringify(val));
    const formGroup = this.fb.group({
        internalId: param.internalId,
        name: param.name,
        description: param.description,
        typeInternalId: param.typeInternalId,
        typeName: param.typeName,
        value: '',
        typeDescription: param.typeDescription
    });
    formGroup.patchValue( { value: val } );
    return formGroup;

  }
}

// let finalValue = '';

// if (param.typeName === 'SiteParam') {
//   let i = 0;
//   for (const siteId of param.value) {
//     if (i !== 0) {
//       finalValue.concat(',');
//     }
//     finalValue.concat(siteId);
//     i++;
//   }
// } else {
//   finalValue = param.value;
// }

// param.value = finalValue;

// console.log('param.toFormGroup: ' + finalValue);
