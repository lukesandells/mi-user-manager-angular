import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { PermissionViewModel, ParamType, SiteViewModel } from '../../shared/param.model';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';

const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'app-permission-item',
  templateUrl: './permission-item.component.html',
  styleUrls: ['./permission-item.component.css']
})

export class PermissionItemComponent implements OnInit {

  @Input() formArray: FormArray;
  @Input() permission: PermissionViewModel;
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() parentIsUser: boolean;

  permGroup: FormGroup;
  index: number;

  @Output() removed = new EventEmitter();

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
      this.permGroup = this.toFormGroup(this.permission);
      console.log('PERMISSION is ' + this.permission.name + ', it has ' + this.permission.parameters.length + ' params');
      resolvedPromise.then(() => {
          this.index = this.formArray.length;
          this.formArray.push(this.permGroup);
      });
  }

  toFormGroup(perm: PermissionViewModel) {
      return this.fb.group({
          internalId: perm.internalId,
          name: perm.name,
          description: perm.description,
          claimType: perm.claimType,
      });
  }

}
