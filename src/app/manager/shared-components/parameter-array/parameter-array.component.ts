import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ParamViewModel, ParamType, SiteViewModel } from '../../shared/param.model';
import { environment } from '../../../../environments/environment.prod';


@Component({
  selector: 'app-parameter-array',
  templateUrl: './parameter-array.component.html',
  styleUrls: ['./parameter-array.component.css']
})
export class ParameterArrayComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() params?: ParamViewModel[] = [];
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() parentIsUser: boolean;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
      if (!this.params) {
        this.params = new Array<ParamViewModel>();
      }
      this.parentForm.addControl('parameters', new FormArray([]));
      console.log('PARAMS length is ' + this.params.length);
  }

  addParam(index: number) {
      this.params.push({
          name: '',
          description: '',
          internalId: environment.emptyGuid,
          value: '',
          typeName: '',
          typeInternalId: '',
          typeDescription: ''
      });
  }

  removeParam(index: number) {
      this.params.splice(index, 1);
      (<FormArray>this.parentForm.get('parameters')).removeAt(index);
  }
}
