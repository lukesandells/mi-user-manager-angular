import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { PermissionViewModel, ParamType, SiteViewModel } from '../../shared/param.model';

@Component({
  selector: 'app-permission-array',
  templateUrl: './permission-array.component.html',
  styleUrls: ['./permission-array.component.css']
})
export class PermissionArrayComponent implements OnInit {

  hasUnassignedPermissions: boolean;
  @Input() parentForm: FormGroup;
  @Input() permissions?: PermissionViewModel[] = [];
  @Input() paramTypes: ParamType[];
  @Input() sites?: SiteViewModel[];
  @Input() existingPermissionsDisplay: PermissionViewModel[] = [];
  @Input() parentIsUser: boolean;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
      this.hasUnassignedPermissions = this.existingPermissionsDisplay.length > 0;
      console.log('HELLO, I have ' + (this.permissions ? this.permissions.length : 'NO') + ' permissions...');
      if (!this.permissions) {
        this.permissions = new Array<PermissionViewModel>();
      }
      this.parentForm.addControl('permissions', new FormArray([]));
      this.parentForm.addControl('existingPermissions', new FormControl(''));
  }

  // addPermission(index: number) {
  //     this.permissions.push({
  //         internalId: '',
  //         name: '',
  //         description: '',
  //         claimType: '',
  //         parameters: [],
  //     });
  // }

  removePermission(index: number) {
      if (this.permissions.length >= 1) {
          const perm = this.permissions[index];
          this.existingPermissionsDisplay.push(perm);
          this.permissions.splice(index, 1);
          (<FormArray>this.parentForm.get('permissions')).removeAt(index);
          this.hasUnassignedPermissions = this.existingPermissionsDisplay.length > 0;
      }
  }

  onAddExistingPermission(permissionId: string) {
    // console.log('EXISTING: ' + permissionId);
    const perm = this.existingPermissionsDisplay.find(p => p.internalId === permissionId);

    this.permissions.push(perm);
    this.existingPermissionsDisplay = this.existingPermissionsDisplay.filter(ep => ep.internalId !== permissionId);
    this.hasUnassignedPermissions = this.existingPermissionsDisplay.length > 0;
  }

}
