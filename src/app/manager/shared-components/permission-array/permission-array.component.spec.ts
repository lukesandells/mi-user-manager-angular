import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionArrayComponent } from './permission-array.component';

describe('PermissionArrayComponent', () => {
  let component: PermissionArrayComponent;
  let fixture: ComponentFixture<PermissionArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
