import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MiUserManagerAppDataService } from '../../../shared/mi-user-manager-app-data.service';
import { UserService } from '../user.service';
import { UserModel, User } from '../user.model';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { RoleViewModel,
        ParamViewModel,
        PermissionViewModel,
        UserViewModel,
        OrganisationViewModel } from '../../shared/param.model';
import { Subject } from 'rxjs/Subject';
import { RoleTemplateService } from '../../role/role.service';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { UserBaseComponent } from '../user-base/user-base.component';
import { PermissionTemplateService } from '../../permission/permission.service';
import { environment } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-create-user',
  templateUrl: '../user-form/user-form.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent extends UserBaseComponent implements OnInit {
  complete = true;
  userInternalId: string;
  pageTitle = 'Create';
  persisted: string = null;
  parentRoleSubject: Subject<RoleViewModel> = new Subject();
  isCreate = true;

  constructor(protected route: ActivatedRoute,
    protected rtSvc: RoleTemplateService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected ptSvc: ParametersService,
    protected userSvc: UserService,
    protected permSvc: PermissionTemplateService,
    protected roleSvc: RoleTemplateService) {
      super(route, userSvc, router, fb, location, ptSvc, permSvc, roleSvc);
    }

  ngOnInit() {
    super.ngOnInit();
    console.log('FML');
  }

  onEditUser() {
    console.log('hit');
    delete this.form.value['roleTemplates'];
    delete this.form.value['paramTypes'];
    delete this.form.value['existingPermissions'];

    const formData = this.form.value;
    const vmToSubmit = new UserViewModel();
    // vmToSubmit.organisation = new OrganisationViewModel();
    vmToSubmit.organisationId = environment.emptyGuid;
    vmToSubmit.email = formData['email'];
    vmToSubmit.firstName = formData['firstName'];
    vmToSubmit.internalId = environment.emptyGuid; // this.userInternalId; // formData['userInternalId'].value;
    console.log('USER INTERNAL ID: ' + vmToSubmit.internalId);
    vmToSubmit.isEnabled = false; // formData['isEnabled'];
    vmToSubmit.lastName = formData['lastName'];

    // vmToSubmit.organisation

    // vmToSubmit.permissions
    for (const permission of formData['permissions']) {
      if (permission['internalId'] && permission['name'] && permission['description'] && permission['claimType']) {
      const perm = new PermissionViewModel();
      perm.claimType = permission['claimType'];
      perm.description = permission['description'];
      perm.internalId = permission['internalId'];
      perm.name = permission['name'];

      for (const permParam of permission['parameters']) {
        if (permParam['typeName'] === 'SiteParam') {
          const pret = super.convertSelectedSitesToString(permParam['value']);
          console.log('param boi ' + pret);
        }
        perm.parameters.push(permParam);
      }

      vmToSubmit.permissions.push(perm);
    }
  }

  // vmToSubmit.roles
  let i = 0;
  for (const role of formData['roles']) {
    console.log('breakpoint here brah');
    if (role['internalId'] && role['name'] && role['description']) {
      console.log(JSON.stringify(role));
      const r = new RoleViewModel();
      r.description = role['description'];
      r.internalId = role['internalId'];
      r.name = role['name'];

      for (const formRoleParam of role['parameters']) {
        if (formRoleParam['typeName'] === 'SiteParam') {
          let finalStr = '';
          let j = 0;
          for (const idStr of formRoleParam['value']) {
            if (j !== 0) {
              finalStr += ',';
            }
            finalStr += idStr;
            j++;
          }

          console.log('final str = ' + finalStr);
          formRoleParam['value'] = finalStr;
        }
        r.parameters.push(formRoleParam);
      }

      // add the role specific permissions
      const rolePermsJson = role['permissions'];
      r.permissions = JSON.parse(rolePermsJson);
      console.log('rolePerms: ' + JSON.stringify(r.permissions));

      console.log('adding role permissions...');

      vmToSubmit.roles.push(r);
    }
    i++;
  }

    console.log('reactive form submitted: ' + JSON.stringify(vmToSubmit));

    this.userSvc.createUser(vmToSubmit)
      .subscribe((data) => console.log(data),
                  err => console.log(err)
      );
  }

}
