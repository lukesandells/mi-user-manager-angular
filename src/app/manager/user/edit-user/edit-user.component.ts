import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MiUserManagerAppDataService } from '../../../shared/mi-user-manager-app-data.service';
import { UserService } from '../user.service';
import { UserModel, User } from '../user.model';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { RoleViewModel, ParamViewModel, PermissionViewModel, UserViewModel } from '../../shared/param.model';
import { Subject } from 'rxjs/Subject';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { UserBaseComponent } from '../user-base/user-base.component';
import { PermissionTemplateService } from '../../permission/permission.service';
import { RoleTemplateService } from '../../role/role.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: '../user-form/user-form.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent extends UserBaseComponent implements OnInit {

  complete = false;
  userInternalId: string;
  pageTitle = 'Edit';
  persisted: string = null;
  parentRoleSubject: Subject<RoleViewModel> = new Subject();
  isCreate = false;
  parameters: ParamViewModel[];
  permissions: PermissionViewModel[] = [];

  constructor(protected route: ActivatedRoute,
    protected userSvc: UserService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected ptSvc: ParametersService,
    protected permSvc: PermissionTemplateService,
    protected roleSvc: RoleTemplateService) {
      super(route, userSvc, router, fb, location, ptSvc, permSvc, roleSvc);
      this.userInternalId = this.route.snapshot.params['internalId'];

  }

  ngOnInit() {
    super.ngOnInit();
    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.userInternalId = parameters['internalId'];
    });

    console.log('InternalID in init is ' + this.userInternalId);

    this.getUser(this.userInternalId);
    this.complete = true;
    // for (const role of this.user.roles) {
    //   for (const param of role.parameters) {
    //     if (param.typeName === 'SiteParam') {
    //       param.value = super.convertSiteIdsToSiteArray(param.value, this.sites).map(sv => sv.internalId);

    //     }
    //   }
    // }
  }

  onEditUser() {

    delete this.form.value['roleTemplates'];
    delete this.form.value['paramTypes'];
    delete this.form.value['existingPermissions'];

    const formData = this.form.value;
    const vmToSubmit = new UserViewModel();

    vmToSubmit.email = formData['email'];
    vmToSubmit.firstName = formData['firstName'];
    vmToSubmit.internalId = this.userInternalId;
    vmToSubmit.isEnabled = formData['isEnabled'];
    vmToSubmit.lastName = formData['lastName'];

    // vmToSubmit.permissions
    for (const permission of formData['permissions']) {
        if (permission['internalId'] && permission['name'] && permission['description'] && permission['claimType']) {
        const perm = new PermissionViewModel();
        perm.claimType = permission['claimType'];
        perm.description = permission['description'];
        perm.internalId = permission['internalId'];
        perm.name = permission['name'];

        // for (const paramId of permission['parameters']) {
        //   for (const param of vmToSubmit.parameters) {
        //     if (paramId === param.internalId) {
        //       perm.parameters.push(param);
        //     }
        //   }
        // }

        for (const permParam of permission['parameters']) {
          perm.parameters.push(permParam);
        }

        vmToSubmit.permissions.push(perm);
      }
    }

    // vmToSubmit.roles
    let i = 0;
    for (const role of formData['roles']) {
      if (role['internalId'] && role['name'] && role['description']) {
        const r = new RoleViewModel();
        r.description = role['description'];
        r.internalId = role['internalId'];
        r.name = role['name'];

//      r.parameters = this.roleAssignments[i].parameters; // role['parameters'];

        for (const formRoleParam of role['parameters']) {
          if (formRoleParam['typeName'] === 'SiteParam') {
            let finalStr = '';
            let j = 0;
            for (const idStr of formRoleParam['value']) {
              if (j !== 0) {
                finalStr += ',';
              }
              finalStr += idStr;
              j++;
            }

            console.log('final str = ' + finalStr);
            formRoleParam['value'] = finalStr;
          }
          r.parameters.push(formRoleParam);
        }

        // for (const roleParam of this.user.roles[i].parameters) {
        //   // if (roleParam.discriminator.length > 0 && roleParam.discriminator.search('Role') !== -1) {
        //     r.parameters.push(roleParam);
        //   // }
        // }

        r.permissions = this.user.roles[i].permissions; // role['permissions'];

        vmToSubmit.roles.push(r);

      }
      i++;
    }

    console.log('reactive form submitted: ' + JSON.stringify(vmToSubmit));
    this.userSvc.editUser(this.userInternalId, vmToSubmit)
      .subscribe((data) => console.log(data),
                  err => console.log(err)
      );
  }
}
