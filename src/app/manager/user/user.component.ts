import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { UserModel, User } from './user.model';
import { MiUserManagerAppDataService } from '../../shared/mi-user-manager-app-data.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { UserViewModel } from '../shared/param.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: UserViewModel[] = new Array<UserViewModel>();
  hasExistingUsers = false;
  complete: boolean;
  user: UserModel;

  constructor(private userSvc: UserService) { }

  ngOnInit() {
    this.userSvc.getUsers().subscribe((tempdate) => {
      this.users = tempdate;
      this.hasExistingUsers = tempdate.length > 0;
    },
    err => console.log(err),
    () => {
        this.complete = true;
        this.hasExistingUsers = this.hasUsers();
      }
    );


    // this.hasExistingUsers = this.hasUsers();
  }

  hasUsers() {
    return this.users.length > 0;
  }

  onUserDelete(userInternalId: string) {
    this.userSvc.deleteUser(userInternalId).subscribe(() => {
        console.log('success');
      },
      () => {
        console.log('failed');
      });
  }

}
