import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MiUserManagerAppDataService } from '../../../shared/mi-user-manager-app-data.service';
import { UserService } from '../user.service';
import { UserModel, User } from '../user.model';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { RoleViewModel, ParamViewModel, PermissionViewModel, UserViewModel, ParamType, SiteViewModel } from '../../shared/param.model';
import { Subject } from 'rxjs/Subject';
import { ParametersService } from '../../shared/parameters/parameters.service';
import { PermissionTemplateService } from '../../permission/permission.service';
import { Observable } from 'rxjs/Observable';
import { RoleTemplateService } from '../../role/role.service';
import { UserManagerBaseComponent } from '../../shared/componentbase/user-manager-base/user-manager-base.component';

const EmailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

@Component({
  selector: 'app-edit-user',
  templateUrl: '../user-form/user-form.html',
  styleUrls: ['./user-base.component.css']
})
export class UserBaseComponent extends UserManagerBaseComponent implements OnInit {

  existingRolesArray: RoleViewModel[];
  existingPermissionsArray: any[];
  sites: SiteViewModel[];
 // paramTypes: ParamType[];
  userSubscription: Subscription;
  user: UserViewModel;
  lastName = new FormControl('', Validators.required);
  firstName = new FormControl('', Validators.required);
  email = new FormControl('', [ Validators.required, Validators.pattern(EmailPattern) ]);
  isEnabled = new FormControl('');
 // form: FormGroup;
  paramsSub: Subscription;
 // complete = false;
  userInternalId: string;
  pageTitle: string;
  persisted: string = null;
  parentRoleSubject: Subject<RoleViewModel> = new Subject();
  isCreate: boolean;

  constructor(protected route: ActivatedRoute,
    protected userSvc: UserService,
    protected router: Router,
    protected fb: FormBuilder,
    protected location: Location,
    protected paramsSvc: ParametersService,
    protected permSvc: PermissionTemplateService,
    protected roleSvc: RoleTemplateService) {
    super(location, paramsSvc);
  }

  ngOnInit() {
    super.ngOnInit();
    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.userInternalId = parameters['internalId'];
    });

    console.log('User InternalID in init is ' + this.userInternalId);
    this.getRoleTemplateParameterTypes();
    this.getExistingPermissions();
    this.getExistingRoles();
    this.getSites();
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email : this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      isEnabled: this.isEnabled,
    });
  }

  getUser(internalId: string) {
    this.userSvc.getUser(internalId)
      .subscribe(
        (user: UserViewModel) => {
          this.onUserRetrieved(user);
        },
        error =>  console.log(error),
        () => this.complete = true
    );
  }


  onUserRetrieved(user: UserViewModel): void {
    if (this.form) {
      this.form.reset();
    }
    this.user = user;

    this.form.patchValue({
      email: this.user.email,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      isEnabled: this.user.isEnabled
    });
  }

  onEditUser() {
    console.log('reactive form submitted');
    this.userSvc.editUser(this.userInternalId, this.form.value)
      .subscribe((data) => console.log(data),
                  err => console.log(err)
      );
  }

  // getRoleTemplateParameterTypes() {
  //   this.paramsSvc.getRoleTemplateParameterTypes()
  //     .subscribe(
  //       (ptype: ParamType[]) => {
  //         this.onRoleTemplateParameterTypesRetrieved(ptype);
  //       },
  //       error =>  console.log(error),
  //       () => this.complete = true
  //   );
  // }

  // onRoleTemplateParameterTypesRetrieved(ptype: ParamType[]): void {
  //   this.paramTypes = ptype;
  // }

  // onReset() {
  //   this.form.reset();
  // }

  // onCancel() {
  //   this.location.back();
  // }

  getSites() {
    // console.log('BLURGH:' + this.parentInternalId);


    this.paramsSvc.getSitesForParameters().subscribe((sites: SiteViewModel[]) => {
          this.onSitesRetrieved(sites);
        },
        error => {
          this.sites = [];
          console.log(error); },
        () => this.complete = true
    );
  }

  onSitesRetrieved(sites: SiteViewModel[]): void {
    this.sites = sites;
  }

  getExistingPermissions() {
    // console.log('BLURGH:' + this.parentInternalId);
    let obs: Observable<PermissionViewModel[]>;

    if (this.userInternalId) {
        obs = this.permSvc.getAllUnassociatedWithUser(this.userInternalId);
    } else {
      obs = this.permSvc.getAll();
    }

    obs.subscribe((existingPermissions: PermissionViewModel[]) => {
          this.onExistingPermissionsRetrieved(existingPermissions);
        },
        error => {
          this.existingPermissionsArray = [];
          console.log(error); },
        () => this.complete = true
    );
  }

  onExistingPermissionsRetrieved(existingPermissions: PermissionViewModel[]): void {
    this.existingPermissionsArray = existingPermissions;
  }

  getExistingRoles() {
    console.log('BLURGH:' + this.userInternalId);
    const obs = this.userInternalId ? this.roleSvc.getAllUnassociatedWithUser(this.userInternalId) : this.roleSvc.getAll();
    obs.subscribe((existingRoles: RoleViewModel[]) => {
          this.onExistingRolesRetrieved(existingRoles);
        },
        error =>  console.log(error),
        () => this.complete = true
    );
  }

  onExistingRolesRetrieved(existingRoles: RoleViewModel[]): void {
    this.existingRolesArray = existingRoles;
  }

}

