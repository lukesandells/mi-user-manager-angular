export class UserModel {
    public InternalId: string;
    public Email: string;
    public FirstName: string;
    public LastName: string;
    public IsEnabled: boolean;

    constructor(internalId: string, email: string, firstName: string, lastName: string, isEnabled: boolean) {
        this.InternalId = internalId;
        this.Email = email;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.IsEnabled = isEnabled;
    }
    // isValid() { return (this.CityCode !== '' && this.CountryCode !== '' && this.RegionKey !== ''); }
}

export interface User {
    InternalId: string;
    Email: string;
    FirstName: string;
    LastName: string;
    IsEnabled: boolean;
}

export interface LoginUser {
    Username: string;
    Password: string;
}
