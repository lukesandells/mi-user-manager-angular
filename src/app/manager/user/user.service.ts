import { Injectable, OnInit } from '@angular/core';
import { UserModel, User } from './user.model';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { UserViewModel } from '../shared/param.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
};

@Injectable()

export class UserService implements OnInit {
  private readonly BaseUrl = environment.userManagerEndpoint + 'ApplicationUser';
  usersSub: Subscription;
  users: Observable<User[]>;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() { }

  getAll() {
    return this.users;
  }

  getUser(internalId: string) {
    return this.httpClient.get<UserViewModel>(this.BaseUrl + '/' + internalId);
  }

  deleteUser(userInternalId: string) {
    console.log('deleting user');
    const params = new HttpParams().set('internalId', userInternalId);
    const headers = httpOptions.headers;

    return this.httpClient.delete<UserViewModel>(this.BaseUrl + '/' + userInternalId, { headers, params});
  }

  createUser(user: UserViewModel) {
    console.log('adding user' + user.email);
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = user;
    return this.httpClient.post<UserViewModel>(this.BaseUrl, body, { headers });
  }

  editUser(userInternalId: string, user: UserViewModel) {
    console.log('editing user');
    const params = new HttpParams().set('internalId', userInternalId);
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = user;
    return this.httpClient.put<UserViewModel>(this.BaseUrl + '/' + userInternalId, body, { headers, params});
  }

  getUsers() {
    console.log('getting users from ' + this.BaseUrl);
    return this.httpClient.get<UserViewModel[]>(this.BaseUrl);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  }

}
