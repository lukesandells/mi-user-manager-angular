import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { MiUserManagerAppDataService } from '../../../shared/mi-user-manager-app-data.service';
import { Subscription } from 'rxjs/Subscription';
import { UserService } from '../user.service';
import { UserViewModel } from '../../shared/param.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userInternalId: string;
  user: UserViewModel;
  paramsSub: Subscription;
  complete = false;

  constructor(private route: ActivatedRoute,
              private userManagerDataSvc: MiUserManagerAppDataService,
              private userSvc: UserService,
              private router: Router) {
    this.userInternalId = this.route.snapshot.params['internalId'];
    // this.user = new UserModel('123', 'Q@a.com', 'Q', 'a', true);
    // this.userSvc.users
    //   .subscribe(
    //     (items: UserModel[]) => { this.user = items.find(p => p.InternalId === this.userInternalId); },
    //     (error) => { console.log(`Error is {{ error }}`); },
    //     () => { this.complete = true; });
  }

  ngOnInit() {
    this.userInternalId = this.route.snapshot.params['internalId'];

    this.paramsSub = this.route.params.subscribe((parameters) => {
        this.userInternalId = parameters['internalId'];
      });

      this.getUser(this.userInternalId);
  }

  getUser(internalId: string) {
    this.userSvc.getUser(internalId)
      .subscribe(
        (user: UserViewModel) => {
          this.onUserRetrieved(user);
        },
        error =>  console.log(error), // err arg
        () => this.complete = true // complete arg
    );
  }


  onUserRetrieved(user: UserViewModel): void {
    this.user = user;
    console.log('Username is ' + this.user.email);
  }

}
