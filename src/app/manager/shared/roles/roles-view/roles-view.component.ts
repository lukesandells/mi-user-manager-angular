import { Component, OnInit, Input } from '@angular/core';
import { RoleViewModel } from '../../param.model';

@Component({
  selector: 'app-roles-view',
  templateUrl: './roles-view.component.html',
  styleUrls: ['./roles-view.component.css']
})
export class RolesViewComponent implements OnInit {

  hasExistingRoles = true;
  complete = true;
  @Input() roles: RoleViewModel[]; // where data is sourced from parent as attribute in HTML
  constructor() { }

  ngOnInit() {
    if (this.roles) {
      this.complete = true;
    } else {
      this.hasExistingRoles = false;
      this.complete = true;
    }

  }

  hasRoles() {
    return this.hasExistingRoles;
  }

}

