import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RolesViewComponent } from './roles-view/roles-view.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [RolesViewComponent],
  declarations: [RolesViewComponent]
})
export class RolesModule { }
