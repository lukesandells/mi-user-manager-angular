import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParamType, SiteViewModel } from '../param.model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ParametersService {

  private readonly BaseUrl = environment.userManagerEndpoint + 'RoleTemplate';
  private readonly SiteUrl = environment.userManagerEndpoint + 'Site';

  constructor(private http: HttpClient) {
  }

  getRoleTemplateParameterTypes() {
    return this.http.get<ParamType[]>(this.BaseUrl + '/GetRoleTemplateParameters');
  }

  getSitesForParameters() {
    return this.http.get<SiteViewModel[]>(this.SiteUrl);
  }
}
