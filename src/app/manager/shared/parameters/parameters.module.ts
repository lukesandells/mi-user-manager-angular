import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ParameterViewComponent } from './parameter-view/parameter-view.component';
import { ParametersService } from './parameters.service';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ ParameterViewComponent, ParametersService],
  declarations: [ ParameterViewComponent, ParametersService],
  providers: [ParametersService]
})
export class ParametersModule { }
