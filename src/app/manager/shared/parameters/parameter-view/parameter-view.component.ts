import { Component, OnInit, Input } from '@angular/core';
import { ParamViewModel } from '../../param.model';

@Component({
  selector: 'app-parameter-view',
  templateUrl: './parameter-view.component.html',
  styleUrls: ['./parameter-view.component.css']
})
export class ParameterViewComponent implements OnInit {

  hasExistingParameters = true;
  complete = true;
  // @Input() parameters: Parameter[]; // where data is sourced from parent as attribute in HTML
  @Input() parameters: ParamViewModel[];

  constructor() { }

  ngOnInit() {
    if (this.parameters) {
      this.complete = true;
      // this.processParameterSource();
    }

  }

  hasParameters() {
    return this.hasExistingParameters;
  }

  processParameterSource() {
    // for (const param of this.parameters) {
    //   const index = param.discriminator.lastIndexOf('.');
    //   param.discriminator = param.discriminator[index + 1];
    // }

    return true;
  }
}
