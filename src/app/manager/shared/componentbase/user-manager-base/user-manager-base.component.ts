import { Component, OnInit } from '@angular/core';
import { ParametersService } from '../../parameters/parameters.service';
import { ParamType, SiteViewModel } from '../../param.model';
import { FormGroup } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-manager-base',
  template: ''
})
export class UserManagerBaseComponent implements OnInit {
  complete: boolean;
  paramTypes: ParamType[] = [];
  form: FormGroup;

  constructor(protected location: Location,
              protected paramsSvc: ParametersService) {
    //
    }

  ngOnInit() {
    this.getRoleTemplateParameterTypes();
  }

  convertSelectedSitesToString(selectedSites: SiteViewModel[]) {
    let j = 0;
    let finalValue = '';
    for (const s of selectedSites) {
      const val: string = s['InternalId'];
      if (j !== 0) {
        finalValue += ',';
      }
      finalValue += val;
      j++;
    }
    return finalValue;
  }

  convertSiteIdsToSiteArray(returnedSites: string, siteList: SiteViewModel[]) {
    const ret: SiteViewModel[] = [];
    returnedSites.split(',').forEach((s) => {
      ret.push( siteList.find(p => p.internalId === s.trim()));
    });

    return ret;
  }

  getRoleTemplateParameterTypes() {
    this.paramsSvc.getRoleTemplateParameterTypes()
      .subscribe(
        (ptype: ParamType[]) => {
          this.onRoleTemplateParameterTypesRetrieved(ptype);
        },
        error =>  console.log(error),
        () => this.complete = true
    );
  }

  onRoleTemplateParameterTypesRetrieved(ptype: ParamType[]): void {
    this.paramTypes = ptype;
  }

  onReset() {
    this.form.reset();
  }

  onCancel() {
    this.location.back();
  }
}
