import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterArrayComponent } from './parameter-array.component';

describe('ParameterArrayComponent', () => {
  let component: ParameterArrayComponent;
  let fixture: ComponentFixture<ParameterArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
