import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ParamViewModel } from '../../../param.model';

@Component({
  selector: 'app-parameter-array',
  templateUrl: './parameter-array.component.html',
  styleUrls: ['./parameter-array.component.css']
})
export class ParameterArrayComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() params: ParamViewModel[];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
      this.parentForm.addControl('parameters', new FormArray([]));
  }

  addEmail(index: number) {
      this.params.push({
          name: '',
          description: '',
          internalId: '',
          value: '',
          typeName: '',
          typeInternalId: '',
          typeDescription: ''
      });
  }

  removeEmail(index: number) {
      this.params.splice(index, 1);
      (<FormArray>this.parentForm.get('parameters')).removeAt(index);
  }

}
