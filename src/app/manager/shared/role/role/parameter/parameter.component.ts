import { Component, OnInit, ChangeDetectorRef, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { ParamViewModel, ParamType } from '../../../param.model';

const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  @Input() formArray: FormArray;
  @Input() parameter: ParamViewModel;
  @Input() paramTypes: ParamType[];
  index: number;

  @Output() removed = new EventEmitter();
  parameterGroup: FormGroup;

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.parameterGroup = this.toFormGroup(this.parameter);

    resolvedPromise.then(() => {
      this.index = this.formArray.length;
      this.formArray.push(this.parameterGroup);
    });
  }

  toFormGroup(param: ParamViewModel) {
    return this.fb.group({
        internalId: param.internalId,
        name: param.name,
        description: param.description,
        typeInternalId: param.typeInternalId,
        typeName: param.typeName,
        value: param.value,
        typeDescription: param.typeDescription
    });
  }
}
