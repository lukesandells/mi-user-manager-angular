import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParameterComponent } from './role/parameter/parameter.component';
import { ParameterArrayComponent } from './role/parameter-array/parameter-array.component';

@NgModule({
  exports: [ ParameterComponent, ParameterArrayComponent ],
  imports: [
    CommonModule
  ],
  declarations: [ ParameterComponent, ParameterArrayComponent]
})
export class RoleModule { }
