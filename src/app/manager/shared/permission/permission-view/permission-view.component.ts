import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-permission-view',
  templateUrl: './permission-view.component.html',
  styleUrls: ['./permission-view.component.css']
})
export class PermissionViewComponent implements OnInit {

  hasExistingPermissions = true;
  complete = true;
  @Input() permissions: PermissionViewComponent[]; // where data is sourced from parent as attribute in HTML
  constructor() { }

  ngOnInit() {
    if (this.permissions) {
      this.complete = true;
    } else {
      this.hasExistingPermissions = false;
      this.complete = true;
    }

  }

  hasPermissions() {
    return this.hasExistingPermissions;
  }

}
