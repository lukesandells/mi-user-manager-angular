import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionViewComponent } from './permission-view/permission-view.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [ PermissionViewComponent ],
  declarations: [ PermissionViewComponent ]
})
export class PermissionModule { }
