import { environment } from '../../../environments/environment';

/*

{
    "InternalId": "2dc540cd-d007-487c-b815-374661a7e831",
    "Name": "Reader",
    "ClaimType": "Read",
    "Description": "Can read",
    "TemplateParameters": [
        {
            "InternalId": "65db26cc-eb52-447f-9a0a-b7894251a4f6",
            "Name": "Read",
            "DefaultValue": "*",
            "Description": "Read parameter",
            "Type": {
                "InternalId": "d21f1147-41f7-4346-987f-17d90ed3f322",
                "Name": "Site param test",
                "Description": "just a test",
                "TypeDiscriminator": "MI.UserManager.Model.SiteParameterType"
            }
        }
    ]
}

*/

// export interface Parameter {
//     InternalId: string;
//     Name: string;
//     Description: string;
//     Discriminator: string;
//     Value: string;
//     Type: ParamType;
// }

export interface ParamType {
    InternalId: string;
    Name: string;
    Description: string;
    TypeDiscriminator: string;
}

// export interface Permission {
//     InternalId: string;
//     Name: string;
//     ClaimType: string;
//     Description: string;
//     Parameters: Parameter[];
// }

export class ParamViewModel {
    constructor() {
        this.name = '';
        this.description = '';
        this.internalId = '';
        this.value = '';
        this.typeName = '';
        this.typeInternalId = '';
        this.typeDescription = '';
    }

    name: string;
    internalId: string;
    description: string;
    value: string;
    typeName: string;
    typeInternalId: string;
    typeDescription: string;
  }

export class RoleViewModel {
  constructor() {
      this.name = '';
      this.internalId = '';
      this.description = '';
      this.parameters = [];
      this.permissions = [];
  }

  internalId: string;
  name: string;
  description: string;
  parameters: ParamViewModel[] = [];
  permissions: PermissionViewModel[] = [];
}

export class UserViewModel {
  constructor() {
      this.email = '';
      this.internalId = environment.emptyGuid;
      this.isEnabled = false;
      this.firstName = '';
      this.lastName = '';
      this.organisationId = environment.emptyGuid;
      this.permissions = [];
      this.roles = [];
  }

  email: string;
  internalId: string;
  isEnabled: boolean;
  firstName: string;
  lastName: string;
  organisationId: string;
  permissions: PermissionViewModel[] = [];
  roles: RoleViewModel[] = [];
  // organisation: OrganisationViewModel;
}

export class OrganisationViewModel {
  constructor() {
      this.internalId = environment.emptyGuid;
      this.name = '';
      this.domain = '';
  }

  internalId: string;
  name: string;
  domain: string;

}

export class SiteViewModel {
  constructor() {
    this.internalId = '';
    this.name = '';
  }

  internalId: string;
  name: string;

}

export class PermissionViewModel {

    constructor() {
      this.name = '';
      this.description = '';
      this.internalId = '';
      this.claimType = '';
      this.parameters = [];
    }

    name: string;
    internalId: string;
    description: string;
    claimType: string;
    parameters: ParamViewModel[] = [];
  }

