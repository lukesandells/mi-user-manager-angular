import { Component, OnInit } from '@angular/core';
import { AuthService } from '../Auth/auth.service';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  firstName: string;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.firstName = this.authService.getFirstName();
  }

}
