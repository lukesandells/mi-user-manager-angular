import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ResetPasswordViewModel } from './reset-password.model';

@Injectable()
export class ResetPasswordService {
  private readonly BaseUrl = environment.userManagerEndpoint  + 'ApplicationUser';

  constructor(private httpClient: HttpClient) { }

  resetPassword(userInternalId: string, user: ResetPasswordViewModel) {
    console.log('editing user');
    const params = new HttpParams().set('internalId', userInternalId);
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = user;
    return this.httpClient.put<ResetPasswordViewModel>(this.BaseUrl + '/ResetPassword/' + userInternalId, body, { headers, params});
  }

}
