import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ResetPasswordService } from './reset-password.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ResetPasswordViewModel } from './reset-password.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  internalId: string;
  paramsSub: any;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private resetPasswordService: ResetPasswordService,
              private router: Router) {

      this.form = this.fb.group({
          oldPassword: ['', Validators.required],
          newPassword: ['', Validators.required],
          confirmNewPassword: ['', Validators.required]
      });
  }

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe((parameters) => {
      this.internalId = parameters['internalId'];
  });
  }

  reset() {
      const val = this.form.value;

      if (val.oldPassword && val.newPassword && val.confirmNewPassword && (val.newPassword === val.confirmNewPassword)) {

          const resetVm = new ResetPasswordViewModel();
          resetVm.OldPassword = val.oldPassword;
          resetVm.NewPassword = val.newPassword;
          resetVm.ConfirmNewPassword = val.confirmNewPassword;

          this.resetPasswordService.resetPassword(this.internalId, resetVm)
              .subscribe(
                  () => {
                      console.log('Password reset');
                      this.router.navigateByUrl('/');
                  }
              );
      } else {
        console.log('All fields must be filled in and new password must match confirm new password');
      }
  }

}
