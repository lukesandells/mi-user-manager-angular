/*
    public class ResetPasswordViewModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
 */

export class ResetPasswordViewModel {
    // public InternalId: string;
    public OldPassword: string;
    public NewPassword: string;
    public ConfirmNewPassword: string;
}
