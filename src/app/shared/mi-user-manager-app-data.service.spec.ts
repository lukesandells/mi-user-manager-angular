import { TestBed, inject } from '@angular/core/testing';

import { MiUserManagerAppDataService } from './mi-user-manager-app-data.service';

describe('MiUserManagerAppDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiUserManagerAppDataService]
    });
  });

  it('should be created', inject([MiUserManagerAppDataService], (service: MiUserManagerAppDataService) => {
    expect(service).toBeTruthy();
  }));
});
