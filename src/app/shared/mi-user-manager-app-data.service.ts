import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { UserModel } from '../manager/user/user.model';
import { UserViewModel, ParamViewModel, RoleViewModel, PermissionViewModel } from '../manager/shared/param.model';

@Injectable()
export class MiUserManagerAppDataService {

  userSelected: Subject<UserViewModel> = new Subject<UserViewModel>();

  user: UserViewModel;
  roles: RoleViewModel[];
  params: ParamViewModel[];
  permissions: PermissionViewModel[];

  constructor() { }

  setUser(user: UserViewModel) {
    this.user = user;
    console.log('user is' + user.internalId);
    this.userSelected.next(this.user);
  }

  getUser(): UserViewModel {
    return this.user;
  }

}
