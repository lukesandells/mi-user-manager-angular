import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ErrorPageComponent } from '../error-page/error-page.component';
import { UserComponent } from '../manager/user/user.component';
import { CreateUserComponent } from '../manager/user/create-user/create-user.component';
import { RoleComponent } from '../manager/role/role.component';
import { RoleDetailComponent } from '../manager/role/role-detail/role-detail.component';
import { PermissionComponent } from '../manager/permission/permission.component';
import { PermissionDetailComponent } from '../manager/permission/permission-detail/permission-detail.component';
import { UserDetailComponent } from '../manager/user/user-detail/user-detail.component';
import { EditUserComponent } from '../manager/user/edit-user/edit-user.component';
import { ManagerComponent } from '../manager/manager.component';
import { LoginComponent } from '../login/login.component';
import { AuthGuardService as AuthGuard } from '../Auth/auth-guard.service';
import { EditPermissionTemplateComponent } from '../manager/permission/edit-permission/edit-permission.component';
import { CreatePermissionTemplateComponent } from '../manager/permission/create-permission/create-permission.component';
import { EditRoleTemplateComponent } from '../manager/role/edit-role/edit-role.component';
import { CreateRoleTemplateComponent } from '../manager/role/create-role/create-role.component';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';

/*const appRoutes: Routes = [
  { path: '', redirectTo: 'manager', pathMatch: 'full' },
  { path: 'manager', component: ManagerComponent, pathMatch: 'full' },
  { path: 'user', component: UserComponent, children: [
    { path: '', component: UserComponent, pathMatch: 'full'},
    { path: 'user/create', component: CreateUserComponent},
    { path: 'detail/:internalId', component: UserDetailComponent},
    { path: 'edit/:internalId', component: EditUserComponent}
  ]},
  { path: 'role', component: RoleComponent, children: [
    { path: '', component: RoleComponent, pathMatch: 'full'},
    { path: 'detail/:roleExternalId', component: RoleDetailComponent}
  ]},
  { path: 'permission', component: PermissionComponent, children: [
    { path: '', component: PermissionComponent, pathMatch: 'full'},
    { path: 'detail/:permissionTemplateExternalId', component: PermissionDetailComponent}
  ]},*/

const appRoutes: Routes = [
  { path: '', redirectTo: 'manager', pathMatch: 'full' },
  { path: 'manager', component: ManagerComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'user', component: UserComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'user/create', component: CreateUserComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'user/detail/:internalId', component: UserDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'user/edit/:internalId', component: EditUserComponent, pathMatch: 'full'/*, canActivate: [AuthGuard]*/ },
  { path: 'role', component: RoleComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'role/create', component: CreateRoleTemplateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'role/edit/:internalId', component: EditRoleTemplateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'role/detail/:internalId', component: RoleDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'permission', component: PermissionComponent, pathMatch: 'full'},
  { path: 'permission/create', component: CreatePermissionTemplateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'permission/detail/:internalId', component: PermissionDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'permission/edit/:internalId', component: EditPermissionTemplateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'error', component: ErrorPageComponent, data: {message: 'An error occurred!'} },
  { path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'} },
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'resetpassword/:internalId', component: ResetPasswordComponent, pathMatch: 'full'},
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
