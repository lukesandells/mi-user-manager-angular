export const environment = {
  production: true,
  tokenEndpoint: 'http://localhost:5000/api/',
  userManagerEndpoint: 'http://localhost:5001/api/',
  emptyGuid: '00000000-0000-0000-0000-000000000000'
};
